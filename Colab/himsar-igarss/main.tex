\documentclass[]{spie}  %>>> use for US letter paper
%\documentclass[a4paper]{spie}  %>>> use this instead for A4 paper
%\documentclass[nocompress]{spie}  %>>> to avoid compression of citations

\renewcommand{\baselinestretch}{1.0} % Change to 1.65 for double spacing


\usepackage{amsmath,amsfonts,amssymb}
\usepackage{graphicx}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{url}


\usepackage[colorlinks=true, allcolors=blue]{hyperref}

\title{HimSAR: A Scientific Toolbox for Snowpack Parameters Estimation}

\author[b]{Surendar Manickam}
\author[a]{Arun Bharathi}
\author[a]{Shaunak De}
\author[a]{Arnab Muhuri}
\author[a]{Abhishek Maity}
\author[a]{Priya Jothi}
\author[a]{Rajesh Jeyakrishnan}
\author[a]{Avik Bhattacharya}
\author[a]{Gulab Singh}
\author[a]{Gopalan Venkataraman}
\author[c]{Snehmani}


\affil[a]{Centre of Studies in Resources Engineering, Indian Institute of Technology Bombay, India}
\affil[b]{Institut f\"{u}r Geographie, Friedrich-Alexander-Universit\"{a}t Erlangen-N\"{u}rnberg, Germany}
\affil[c]{Snow and Avalanche Study Establishment (SASE), DRDO, India}

\authorinfo{This work was supported by Department of Science \& Technology (DST), Government of India \\Further author information: (Send correspondence to S.M. or S.D.)\\S.M: E-mail: surendarmtech@gmail.com\\  S.D.: E-mail: shaunak.de@ieee.org}

% Option to view page numbers
\pagestyle{empty} % change to \pagestyle{plain} for page numbers   
\setcounter{page}{301} % Set start page numbering at e.g. 301
 

 
\begin{document} 
\maketitle

\begin{abstract}
In this paper, we present \emph{HimSAR}: a toolbox for  snow pack parameter estimation from Synthetic Aperture Radar (SAR) data. This is indigenously developed at the Microwave Remote Sensing Lab ($mrslab.in$), at the  Centre of Studies in Resources Engineering (CSRE), Indian Institute of Technology Bombay, India. Implementations of recently developed algorithms for the estimation of snow wetness, snow density, and snow surface dielectric constant are the main components in the toolbox. Additionally, various existing and proposed classification and decomposition techniques utilizing polarimetric SAR (PolSAR) data are also included. The main objective of this toolbox is to provide a flexible, friendly and comprehensive environment to users utilizing PolSAR data for snowpack parameter studies.
\end{abstract}

% Include a list of keywords after the abstract 
\keywords{PolSAR, Software Toolbox, Cryosphere}

\section{Introduction}
\label{sec:intro}
Remote sensing using Synthetic Aperture Radar (SAR) empowers the scientific community to obtain all-weather, year-round monitoring of the Earth's surface.  Present and upcoming SAR sensors have an objective for monitoring of Earth's snow-cover and glaciers. This will lead to large amounts of generated data. It is necessary to have a toolbox for efficient and quick processing of PolSAR data for cryospheric studies. \emph{HimSAR} is an anglicized portmanteau of the Devanagari word `him' meaning snow and SAR. It is a stand-alone toolbox composed of newly proposed snowpack parameter estimation techniques along with other recently developed PolSAR data processing algorithms. 
This toolbox contains algorithms for snowpack parameters estimation from dual and full polSAR data obtained from ALOS PALSAR/ALOS-2, RADARSAT-2 and TerraSAR-X radar sensors. Additionally, it also provides the conventional SAR processing algorithms, and commonly used PolSAR decomposition techniques.

\section{Principal Objectives and Context}
\label{sec:format}
Inspired by the success of the PolSARpro~\cite{pottier2012polsarpro} software, for increasing accessibility to the PolSAR community, motivated us to develop a special tool box for snowpack studies. The objective of this project is to provide a flexible tool focused for the scientific community working on Earth's cryosphere and to make it easy for them to leverage PolSAR techniques for their application. Figure~\ref{fig:welcome} shows the \emph{HimSAR} main entry screen. 

\section{Software Portability And Development Language}
\label{sec:pagestyle}

Users ranging from novices to experts will find this toolbox easy install and use. This software is written in Qt, a modern cross-platform framework in C++, allowing it to be run on a variety of system environments: Windows, Linux and Mac. The toolbox being a Graphical User Interface (GUI) based application enables user to select, set, experiment parameter values and run the software. The \emph{HimSAR} application acts as a complimentary toolbox to the existing open-source SAR package like PolSARpro and commercial ones like ENVI etc. 

\begin{figure}
\centering
  \includegraphics[width=0.6\columnwidth]{figures/welcome.pdf}
  \caption{Proposed welcome screen for \emph{HimSAR} application}
  \label{fig:welcome}
\end{figure}

\emph{HimSAR} is comprised of a single document interface (SDI) mainframe window which incorporates the title, menu, status, help bars and process-dependent dialog boxes. The dialog windows pop-in when the user triggers the specific menus from the menu bar. The processing is controlled primarily using these dialog windows. The software multithreaded to improve speed of operation and stability. The task is distributed in a work queue to several threads allowing the computation to be performed on typically large SAR data files in parallel. \emph{HimSAR} is written in a modular structure and has a large number of reusable user defined functions. The ability of the C++ programming language to perform dynamic memory allocation has been leveraged to improve memory management and ensure that system resources are used optimally. This also makes the software light and scalable on a wide variety of hardware configurations.  

The software is licensed under the `Apache License 2.0' with the source code made freely available to the public~\cite{apache2}.  The toolbox follows `spiral-model' of software development. This will allow researchers to add new modules and extend the software over time, enhancing the productivity and versatility of toolbox or adapt the internal routines to their own needs. The software does not use any component derived from commercial processing packages and has no licensing requirements in any environment.  
 
\section{Main menu functionality}
\label{sec:typestyle}
\emph{HimSAR} is developed to handle polarimetric data available from operational PolSAR spaceborne sensors, namely ALOS PALSAR, ALOS-2, RADARSAT-2 and TerraSAR-X. These can be located in the drop-down options from main menu under `SAR data' button. The \emph{HimSAR} package has been developed to support the data sources given in Table~\ref{table:sensor}

\begin{table}
\centering
\begin{tabular}{c c c}
		\hline
		\textbf{Mission} & \textbf{Sensor} & \textbf{PolSAR data type} \\ \hline
		ALOS - PALSAR & PALSAR (Fine mode) & Dual-Pol \\
		& PALSAR (Polarimetry mode) & Quad-Pol \\
		{RADARSAT-2} & SAR (selective polarisation) & Dual-Pol \\
		& SAR (standard quad, fine quad) & Quad-Pol \\  
		TerraSAR-X & TSX-SAR & Dual-Pol \\
	\end{tabular}%

 \caption{Sensors and Data Types}
 \label{table:sensor}
\end{table}

The  application has a collection of methods and algorithms for the processing and analysis of PolSAR single data sets and dual data sets. The other main functionality of the main menu are described in Table~\ref{table:menu}. A general view of the application with menu bar and functions running in different operating systems are given in Figures~\ref{fig:dialog} (Windows) and~\ref{fig:linux} (Linux). Currently, the Macintosh build is not compiled by the team.  

\begin{table}
\centering

		\begin{tabular}{ l l }
			\hline
			\textbf{Menu title} & \textbf{Functionality} \\ \hline
			\multirow{2}{*}{SAR Data} & Input for ALOS 1 and ALOS 2 data \\ & RADARSAT-2 and TerraSAR-X \\
			\hline
			\multirow{1}{*}{Calibration} & Perform calibration for the above data formats \\
			\hline
			\multirow{6}{*}{Process} & Generation of C2 matrix with speckle filtering add-on. \\
			& Generation of T3 matrix with speckle filtering add-on \\
			& Classification involving the H/A/$\alpha$ and Wishart classification. \\
			& Decomposition processes involving \\ & Freeman three component~\cite{freeman1998three}, Yamaguchi four component~\cite{yamaguchi2005four}, \\ & Singh four component~\cite{singh2013general}, SD-Y4O decompositions~\cite{bhattacharya2015modifying}.\\ & Adaptive G4U decompositions~\cite{bhattacharya2015adaptive} \\
			\hline
			\multirow{5}{*}{Snow Pack} & Snow wetness involving full-pol, dual-coherent data and\\ & Shi Dozier function~\cite{shi2000estimation}. \\
			& Snow density involving full-pol data and Shi Dozier function \\
			& Snow surface dielectric using DOP alpha S1 based method\\
			& Calculate Radar Snow Index\\
			\hline
			\multirow{1}{*}{Post Processing} & Pixel Masking and Layover Shadow. \\
			\hline
			\multirow{1}{*}{Settings} & For application general settings. \\
		\end{tabular}%
        
    \caption{Menu and Functionalities}
    \label{table:menu}
\end{table}



\section{Data Format Standards}
\label{sec:majhead}

\emph{HimSAR} can process a variety of PolSAR data sets.  ALOS PALSAR L-band data is supplied in the CEOS format. The $(2\times2)$ complex Sinclair [S2] matrix and the metadata are extracted from the VOL and LED files respectively. RADARSAT-2 is a C-band sensor with the product supplied in \emph{xml} format. The product consists of a descriptive product.xml, and imagery supplied in the TIFF format. Calibration data is supplied in the lutSigma.xml, lutBeta.xml, lutGamma.xml files. These \emph{xml} files are used to process the data in the application to generate the output matrix. TerraSAR-X is a X-band dual-coherent PolSAR sensor and whose data is also supplied in an \emph{xml} format with the image data contained in the `.cos' file. 
For each of these sensors, \emph{HimSAR} ingests the image files and metadata and converts it to an intermediary data format consisting of a binary (bin) file and a configuration (config) text file. The $\sigma_{0}$ calibration is then executed by power or dB image option(s). 

\begin{figure*}[t]
\centering
	\includegraphics[width=0.8\textwidth]{figures/dialog.png}
	\caption{Screen-shot of \emph{HimSAR} Windows application}
	\label{fig:dialog}
\end{figure*}

\begin{figure}[h]
\centering
    \includegraphics[width=0.8\columnwidth]{figures/dialog_linux.png}
    \caption{Snow surface dielectric dialog box in Linux OS}
    \label{fig:linux}
\end{figure}

\section{Snowpack Algorithms}
\label{sec:snowpack}
In-house developed snow wetness~\cite{surendar2015development} snow density~\cite{surendar2015estimation}, snow surface dielectric constant~\cite{manickamestimation} and snow cover map~\cite{Singh2014} algorithms are the main contribution to this toolbox. Polarimetric SAR data is used to derive the generalized surface and volume scattering parameters over the snowcover area. These parameters are then inverted as a snow surface and volume dielectric constants. These snow surface and volume dielectric constants are then used for the estimation of snowpack wetness and density. The dominant scattering type magnitude proposed in~\cite{Touzi2007} and the Adaptive Generalized Unitary (AGU) transformation based optimum degree of polarization proposed in~\cite{bhattacharya2015adaptive} are used for the estimation snow surface dielectric constant~\cite{manickamestimation}. Polarization fraction based snow cover mapping approach is proposed in~\cite{Singh2014}. Apart from these other existing snowpack parameter estimation algorithms proposed in~\cite{shi2000estimation,Shi_Wetness} are also included in this toolbox. 

\section{Interface with External Software}
\label{sec:external}
The internal formats used by \emph{HimSAR} are compatible with PolSAR Pro, SNAP, ENVI® and other commonly used toolboxes making it possible to interchangeably use it with other toolboxes and easily integrate it into existing workflows. Additionally it can also export map data to Google Earth for exploration and visualization. 
The simple text and binary file based data-structure makes it easy to read the products in common programming environments such as Matlab®  and Python. 

\section{Release Status}
\label{sec:Viewpoint}
\emph{HimSAR} is currently in its final-development stages with new software tools and modules being gradually added since 2015. The \emph{beta} version is proposed to be released soon to the public via the Microwave Remote Sensing Lab website:

\begin{center}
\url{http://www.mrslab.in/himsar.html}
\end{center}

\section{Future Expansion}
\label{sec:future}
In the future we intend to add support for more graphical and visualization toolkits. We also plan to argument functionality like snow cover mapping, glacier velocity estimation and the newly proposed relative decorrelation based Y4O decomposition (RD-Y4O)~\cite{ratha2017improvement} and co-registering of Single Look Complex (SLC) PolSAR images etc. We also propose to develop a viewer or light version of \emph{HimSAR} that can be run on Android OS making this a highly portable application. We also plan to include machine learning techniques for PolSAR image classification~\cite{de2015urban}, segmentation etc.

\section{Acknowledgment}
\label{sec:ack}

The authors would like to thank DST, SASE-DRDO for providing financial help and ground facility to validate the proposed algorithms respectively. We would also like to thank CSA \& MDA corporation for providing RADARSAT-2 data under AO program, JAXA for ALOS PALSAR data and DLR for TerraSAR-X data. All copyrights belong to their respective owners. MATLAB® is a registered trademark of The MathWorks, Inc. ENVI is a registered trademark of the Exelis Visual Information Solutions, Inc., a subsidiary of Harris Corporation

% References
\bibliography{report} % bibliography data in report.bib
\bibliographystyle{spiebib} % makes bibtex use spiebib.bst

\end{document} 
