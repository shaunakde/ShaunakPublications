\section{Methodology}\label{method}

The methodology consists of two parts, first the PolSAR data is transformed into basis simulated normalized stokes vectors (Section~\ref{sec:poincare_A5x}), followed by encoding and classification by an Auto-Encoder network (Section~\ref{sec:AE_A5x}).

\begin{figure*}[!htbp]
\centering
\includegraphics[width=0.6\textwidth]{Figure/FlowChart}
\caption{Overall description of the Neural Network.}
\label{fig:flow}
\end{figure*}


\subsection{Polarization Basis}
\label{sec:poincare_A5x}
In PolSAR, the Mueller matrix is a real representation of backscattering measurement unlike scattering or covariance matrices which are complex. The Mueller matrix for incoherent scattering representation can be obtained from the elements of the coherency matrix $[\mathbf{T}]$ as follows:

\begin{equation}\label{incoKen}
[\mathbf{M}] =\resizebox{0.9\hsize}{!}{$
\left[\begin{array}{cccc}
\frac{T_{11}+T_{22}+ T_{33}}{2} & \Re(T_{12}) & \Re(T_{13}) & \Im(T_{23})\\
\Re(T_{12}) & \frac{T_{11}+T_{22}-T_{33}}{2} & \Re(T_{23}) & \Im(T_{13})\\
\Re(T_{13}) & \Re(T_{23}) & \frac{T_{11}-T_{22}+T_{33}}{2} & - \Im(T_{12})\\
\Im(T_{23}) & \Im(T_{13}) & - \Im(T_{12}) &\frac{-T_{11}+T_{22}+T_{33}}{2}
\end{array}\right]
$}
\end{equation}

The received Stokes vector $\begin{bmatrix} g_{0r} \; g_{1r} \; g_{2r} \; g_{3r} \end{bmatrix}^{T}$~\eqref{eq:stokes} is directly obtained from the observed Mueller matrix $[\mathbf{M}]$ by altering the incident Stokes vector $\begin{bmatrix} g_{0i} \; g_{1i} \; g_{2i} \; g_{3i} \end{bmatrix}^{T}$ given in table~\ref{tab:K_models}.

\begin{gather}
 \begin{bmatrix} g_{0r} \\ g_{1r} \\ g_{2r} \\ g_{3r} \end{bmatrix}
 =
  [\mathbf{M}] \cdot
 \begin{bmatrix} g_{0i} \\ g_{1i} \\ g_{2i} \\ g_{3i} \end{bmatrix}
 \label{eq:stokes}
\end{gather}

The normalized Stokes vector is given in~\eqref{eq:norm_stokes}~\cite{Shang_QNN_2014} indicates a point on or inside the Poincar\'e sphere depending on the degree of polarization. This normalized vector is used as an input feature to the neural network, $X$. 

\begin{equation}
X = (\bar{g}_{1r}, \bar{g}_{2r}, \bar{g}_{3r}) = \left(\frac{g_{1r}}{g_{0r}}, \frac{g_{2r}}{g_{0r}}, \frac{g_{3r}}{g_{0r}} \right)
\label{eq:norm_stokes}
\end{equation}

\begin{table}[tp]
\centering
\caption{Incident Stokes vector representation (H: Linear horizontal, V: Linear vertical, R: Right circular, L: Left circular)}
\label{tab:K_models}
\begin{tabular}{lcccc}
%\toprule
$g_{i}$ & H & V & R & L\\ %\midrule
$g_{0i}$ & 1 & 1 & 1 & 1 \\ 
$g_{1i}$ & 1 & $-1$ & 0 & 0 \\ 
$g_{2i}$ & 0 & 0 & 0 & 0 \\ 
$g_{3i}$ & 0 & 0 & $-1$ & $1$ \\ 
%\bottomrule
\end{tabular}
\end{table}

\subsection{Features Generation}

- how basis change can make features
- What this represents physically
- talk about + refer ideal basis for each target

\subsection{Auto-encoder network}
\label{sec:AE_A5x}
The neural network is comprised of two parts, an unsupervised AE for feature representation learning, and a supervised Feed-Forward (FF) network for supervised classification as shown in Fig.~\ref{fig:flow}. 
%
The AE has five layers: two encoding layers, two decoding layers and one representational layer, whose weights are denoted as $Z$. During training of the AE encoder, the FF network is disconnected i.e. weight updates for it are suspended. Rectified linear unit (ReLU) is used as the activation function to overcome the issue of vanishing gradients during training in a deep network.
%
The goal of the AE is to learn a representation $X'$, given input $X$. It follows that $X$ must be as close to $X'$ as possible. This governed by the cross entropy error between them during the training phase give as,

\begin{equation}
H(X,X') = E_X[-log X'] = H(X) + D_{KL}(X||X')
\end{equation}

where $H(X)$ us the entropy of $X$, and $D_{KL}(X||X')$ is the  Kullback-Leibler divergence of $X$ from $X'$.

Training of the AE is considered complete if the  change in cross entropy error between successive iterations is negligible, or if the maximum number of iterations are exceeded. The pixels must be presented in a different randomized order each epoch, to prevent the network from memorizing the order pixels, and instead try to learn an optimal representation to simplify classification of the data.
%
After the training phase of the AE, the representational layer is extracted by applying the Poincar\'e sphere parameters and saving the weights of $Z$, in a pixel-by-pixel fashion. These extracted weights are used as input for the FF network. The AE is set up such that the dimensionality of $Z$ is greater than that of $X$. This makes the representation sparse, which can lead to improved separability between classes~\cite{coates2011importance}. 

The FF network is a three layer fully connected supervised feed forward network. The extracted representation $Z$ is given as input along with training labels to the FF network. It is trained in a supervised manner with Sigmoid nodes used as non-linearities. The training in this case is governed by the $L2$ loss. After training, the unlabeled pixels are presented to the network, and the output response is monitored. The pixel is classified into the class corresponding to the node with the highest response. 
%
Thus, a sparse unsupervised representation is learned by an AE, followed by supervised classification of this representation by a FF network. The representation is more separable than the original feature space which improves classification performance. In this paper, we use both full and hybrid polarimetric data to evaluate their effectiveness in snow cover mapping application. 

\section{Experimental Setup}

For the AE, the learning rate is $l_r = 10^{-4}$ with a reduction by a factor of $\Gamma = 0.1$ at every 2 training epochs. Optimization is performed using the ADAM solver with a momentum $\mu = 0.95$ and weight decay $\beta = 0.005$. The total labeled data is divided into 3 parts: training, test and validation which are selected at random. The proportions are 10\%, 30\% and 60\% of the total labeled data respectively. The training is balanced by selecting a fixed number of pixels per class randomly from the 10\% training pool. The accuracy statistics are reported as an average of 5 runs on the validation set. The FF network is trained in a supervised manner with $l_r = 10^{-5}$ , $\mu = 0.92$ and $\beta = 0.005$. The classification is obtained as output from the terminal layer of the FF network. 

% % CAN BE REMOVED FOR IGARSS
The training is proceeded under constraint of the cross-entropy error, which is monitored individually for the training and test datasets. If the cross-entropy error of the two datasets follow very nearly the same trajectory, it can be deduced that the network is under-fitting the dataset. On the contrary, if the trajectories are held at a constant, but significant offset, with the training leading the test, the network is over-fitting the data, leading to a better fit in the training samples, but poor performance in the test-set. In the ideal case, the training leads the curve of the test cross-entropy error with a moderate margin. The aforementioned hyper-parameters are tuned to achieve the same. 