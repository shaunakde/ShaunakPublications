% Template for IGARSS-2016 paper; to be used with:
%          spconf.sty  - LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{spconf,amsmath,epsfig}

\usepackage{graphicx}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{bm}
\usepackage{tabularx}

\usepackage{subcaption}

% Example definitions.
% --------------------
\def\x{{\mathbf x}}
\def\L{{\cal L}}

% Title.
% ------
\title{Classification of Urban Areas from Polarimetric SAR data using a Deep Learning Algorithm}
%
% Single address.
% ---------------
\name{Shaunak De and Avik Bhattacharya}
\address{Centre for Studies in Resources Engineering\\ Indian Institute of Technology Bombay\\Powai, Mumbai 400076, India}
%
% For example:
% ------------
%\address{School\\
%	Department\\
%	Address}
%
% Two addresses (uncomment and modify for two-address case).
% ----------------------------------------------------------
%\twoauthors
%  {A. Author-one, B. Author-two\sthanks{Thanks to XYZ agency for funding.}}
%	{School A-B\\
%	Department A-B\\
%	Address A-B}
%  {C. Author-three, D. Author-four\sthanks{The fourth author performed the work
%	while at ...}}
%	{School C-D\\
%	Department C-D\\
%	Address C-D}
%
\begin{document}
\ninept
%
\maketitle
%
\begin{abstract}
This paper proposes a deep learning based algorithm for the accurate classification of urban areas in PolSAR datasets. 
\end{abstract}

\begin{keywords}
One, two, three, four, five
\end{keywords}

\section{Introduction}
\label{sec:intro}


%Polarimetric SAR (PolSAR) imaging is an advancement made on single polarization SAR imaging. The polarization of the incident wave is controlled so that it is restricted alternatively to either H or V transmission. Similarly, reception is performed alternativly in H or V polarizations, so that for every imaged pixel, four combinations of polarization i.e. HH, HV, VH and VV are captured. PolSAR imaging can capture more information about the physical properties of the target than possible with single or dual polarization imaging, and consequently, this can lead to better classification~\cite{Lee2001multipolclass}. The primary challenge with the classification of urban areas in PolSAR data however are those regions that are rotated away from the radar line of sight. The areas perpendicular to the radar line of sight manifest even bounce scattering, as expected, but the immediate surrounding areas which are at a slight angle exhibit volume (cross-polarized) scattering.

%The first attempt at the classification of polarimetric SAR images was made by~\cite{kong1988identification}. They proposed a Maximum Likelihood based classification method for a PolSAR image based on a complex Gaussian distribution. They showed that the addition of information from the HH-VV phase improves the classification accuracy. Subsequently, many research institutions devoted more time and effort to exploring the possibilities in the classification of PolSAR images. A Neural Network classifier for SAR data was proposed by~\cite{pottier1991radar}. They used a feed forward network with a single hidden layer with a sigmoidal discriminant element. To account for the spatial corelation of pixels,~\cite{rignot1992segmentation} used model for the conditional distribution of PolSAR data in combination with a Markov Random field representation for the distribution of the region labels. They then used a maximum a posteriori (MAP) estimate to optimize the classification. When PolSAR data is represented in the form of a complex co-variance matrix, it has a complex wishart distriution. A maximum likelihood classifier based on the distribution was developed to perform terrain classification by~\cite{lee1994classification}. This went on to become one of the most widely used SAR classification techniques for PolSAR data and many following algorithms were based on this scheme. Later an unsupervised technique was also developed that used a target scattering decomposition, namely ~\cite{freeman1998three}, and a maximum likelihood classifier based on the above technique~\cite{lee2004classification}. Another popular unsupervised approach was proposed in~\cite{964969}. It uses the H/A/$\alpha$ decomposition parameters to perform segmentation. In 2006,~\cite{hinton2006fast} introduced the concept of deep learning, which has been well received with enthusiasm and fervor both in industry and academia. Deep learning is a new field of machine learning research, the motivation lies in the establishment neural network which simulate the way the human brain works. It has shown immense potential in the field of  speech recognition, image recognition, natural language processing and text searching amongst others. Deep learning holds enormous promise for SAR classification and recognition tasks.  







In this study we classify polarimetric SAR (PolSAR) images to identify urban areas and separate them from their surrounding land-cover classes like forest, bare-soil and water bodies. Segmented urban area maps can subsequently be used to characterize urban density, land-area usage, urban sprawl~\cite{yeh2001measurement} and land cover change in temporal data amongst other applications. 

Challenges in classification of PolSAR data arise from the usual representation of the data as a complex hermitian matrix, the presence of speckle noise, large dynamic range of the data and the behavior of targets, especially urban areas, that are oriented away from the radar line of sight. PolSAR data is usually represented as a coherency $\bm{T3}$ or covariance $\bm{C3}$ matrix which are complex hermitian positive semi-definite matrices. The presence of complex valued elements prevents direct use of many machine learning algorithms; especially ones which involve back-propagation because in the complex space the solution of differentiation is not guaranteed to be analytic. One possible work-around involves separating the real and imaginary parts, but this leads to a loss in information available to the learning technique. In this study, we transform the data to it's Mueller representation. It comprises of real numbers making the training of the learning algorithm simpler.  Speckle noise in SAR images are caused as a consequence of the imaging medium's coherent nature~\cite{lee1981speckle}. Speckle appears as significant bright and dark variations, even in homogeneous areas. In our methodology we use a refined lee filter~\cite{lee1981refined} to suppress the effect of speckle. Repeated random iterations in the training process also help achieve better generalization ability which allows the learning machine to deal with speckle noise in a more effective manner. Dynamic range of radar images is significantly larger than other imaging sensors. The noise equivalent $\sigma_0$ for the UAVSAR's L-band sensor is about $-45 dB$~\cite{Hensley2008}. It becomes a challenge to deal with such a large range of floating point values directly. In our method, we take advantage of a deep learning approach, where the first part of the network is an auto-encoder which learns a simplified representation of the data in the feature space. This makes the problem of classification easier to solve in subsequent stages. 

Perhaps the challenge that is unique to the problem of classification of urban areas, or even bounce scattering in general, with PolSAR data, is the behavior of targets when oriented away from the line of sight of the radar. As seen in Figure~\ref{fig:orientationExample}, urban targets that are rotated away from the radar line of sight appear to exhibit volume scattering, due to the presence of cross-polarization instead of the expected even bounce scattering. For example area, in Figure~\ref{fig:orientationExample}, $'A'$ show up as green in the Pauli representation instead of red like area $'B'$ because of the orientation with respect to the range direction of the radar, despite both areas being similar in nature. To overcome this problem, we synthetically rotate the input $\bm{T3}$ matrix to obtain a more general representation of the target, which is then used in training the auto-encoder. As shown in subsequent sections, this can help the learning-machine identify urban targets of similar nature, but oriented at different angles from what it encounters in training, improving the accuracy of the final classification. 

\section{Methodology}
The classification process can be split into three stages. In the first stage, the PolSAR data is processed and synthetic rotations are applied. The second stage consists of the auto-encoder which automatically learns the features from the input and synthetically generated data. Finally, in the third stage the features extracted from the preceding stage are classified in a supervised manner using a multi-layer perceptron (MLP) network.  

\subsubsection*{Stage I}
%\textsc{Stage I}

\begin{figure}
\centering
\includegraphics[width = \columnwidth]{Figures/Trento/Method1}
\caption[PolSAR data preprocessing Stage 1]{Stage I - Schematic diagram of the PolSAR data pre-processing, synthetic rotation and conversion to Mueller steps.}
\label{fig:method1}
\end{figure}

The first stage of the classifier is represented in Figure~\ref{fig:method1}. PolSAR data is made available from the sensor as a calibrated scattering matrix as described in Equation~\ref{eqn:scattring}. To utilize polarimetric information, we express the data as a spatially averaged coherency $\langle\bm{T3}\rangle$ matrix. Radar images, as a consequence of their coherent nature, are subject to a speckle pattern~\cite{lee1981speckle}. To suppress this, a Refined Lee speckle filter~\cite{lee1981refined} is applied to the dataset as a pre-processing step. This filter is based on the local statistics of the data, is effective, and has a good edge preservation ability allowing the retention of details~\cite{lee1994speckle}. The filtered $\bm{T3}$ matrix is then rotated by $\theta$ varying from $-22^\circ$ to $22^\circ$ as described in the Section~\ref{sec:rotation}. Each rotated $\bm{T3}$ matrix is then converted to it's Mueller representation and the matrices are collated together for input to the next stage of the classifier. A detailed description of the rationale behind the conversion to Mueller and rotation of the $\bm{T3}$ matrices are presented in Section~\ref{sec:stage1}

%Radar images, as a consequence of their coherent nature, are subject to a speckle pattern~\cite{lee1981speckle}.	
%PolSAR data is made available from the sensor as a calibrated scattering matrix as described in Equation~\ref{eqn:scattring}. Radar images, as a consequence of their coherent nature, are subject to a speckle pattern~\cite{lee1981speckle}.	

\subsubsection*{Stage II}
%\textsc{Stage II}

An auto-encoder is a neural network that can automatically learn an alternate representation of the input data in a feature space. When used in a sparse configuration, it has been shown that the network is able to learn useful representations of the data, which are easier to classify then the original data-space~\cite{hinton2006fast}.   These features show favorable characteristics like lower dynamic range and better generalization capability than the original data-set. Also, the use of an auto-encoder allows the incorporation of synthetically generated datasets, i.e. the rotated $\bm{T3}$ matrices, which improves information content and classification accuracy. Back-propagation algorithm used in the training of neural networks does not have an analytic solution for complex values~\cite{hirose2006complex}. By transforming the data to the Mueller representation we guarantee convergence of the auto-encoder. In this study we use a 6 layer sparse stacked auto-encoder as shown if Figure~\ref{fig:method2}. 

\begin{figure}
\centering
	\includegraphics[width = \columnwidth]{Figures/Trento/Method2}
	\caption[PolSAR data preprocessing Stage 2]{Stage II: Schematic diagram of the Auto-Encoder stage which is used for automatic feature learning. Layer 6 is extracted as the final feature-set for input to the next stage. }
	\label{fig:method2}
\end{figure}

\subsubsection*{Stage III}
%\textsc{Stage III}

The final stage of the classifier comprises of a Multi Layer Perceptron (MLP) that takes the features from the preceding stage, statistical parameters extracted from matching the $G^0$ distribution and supervised training labels to produce the final classified dataset. As shown in Figure~\ref{fig:method3}, this stage is a 3 layer network with sigmoid non-linearities. 

\begin{figure}
\centering
	\includegraphics[width = \columnwidth]{Figures/Trento/Method3}
	\caption[PolSAR data preprocessing Stage 3]{Stage III: Final classification is done using a Multi-Layer Perceptron network with some physical parameters extracted from the data.}
	\label{fig:method3}
\end{figure}
	

\section{Study Area and Data Sets}
%The San Francisco bay area consists of highly urbanized structures, ranging from moderate to low density developed areas. This area is characterized by well planned residential and commercial areas, which appear as a very regular repeated pattern in the SAR image. In the area, entire neighborhoods are planned at slight angles to others, which cause it to be oriented differently, away or towards, the radar line of sight as compared to the surrounding urban structures.  The urban land-cover is surrounded by forest-cover consisting of mainly deciduous trees growing on hilly terrain around the central bay. This also tends to exhibit a volume scatter and can be easily confused with the oriented urban area. In this study, we use data-takes Radarsat-2 operating in Fine Quad mode acquired over San Francisco on XXX with a resolution of 8m in range and azimuth. 

\section{Results}
\label{sec:results}

The Radarsat-2 C-Band dataset collected over San Francisco, CA, is classified using the described methodology. The result is shown in Figure~\ref{fig:output1}. Training areas for the classification are selected from topographical maps of the area, interpretation of aerial images and the Pauli composition derived from the radar dataset. The selected ground-truth regions are marked in Figure~\ref{fig:train1} with red polygons for urban classes, green for forest and blue for water superimposed on the Pauli composition image. The proportion of ground-truth pixels in each class is approximately the same, and in total, they constitute less than $\sim10\%$ of the total pixels in the image. From these ground-truth pixels, $40\%$ of the pixels are randomly selected as training pixels, and the rest are reserved for test. A testing phase is run every 1000 iterations which measures the accuracy of prediction over this labeled set. **This is plotted**. 

To quantify the performance of the classifier outside of the labeled pixels validation areas are selected separate from the areas used in the training. A confusion matrix is computed for these validation pixels and is tabulated in Table~\ref{tab:conf1}. The data is classified with an overall accuracy of  $\sim92\%$ and a Kappa coefficient of $0.84$. Kappa is a measure of the confidence of the classifier which ranges from a low confidence score of 0, to a maximum of 1. Urban areas are most confused with Forest class, since urban structures aligned at an angle to the radar line of sight exhibit similar scattering characteristics as that of vegetation. This effect is however not reciprocal as the probability of a forest pixel being misclassified  as an urban area is significantly lower. Since the penetration of the radar is higher for vegetation and other natural targets, it is an easier target to discern for a fully polarimetric sensor~\cite{ainsworth2009classification}. The water class is easily classified with no error of omission and low errors of commission. The even bounce scattering mechanism of this class type makes it easily discernible from the other two. 

\begin{table}[!h]
\centering

\caption{Confusion matrix and accuracy statistics for Radarsat-2 San Francisco, CA data-set using the proposed method.}
\label{tab:conf1}
\begin{tabularx}{\columnwidth}{X|XXX}
 & Urban & Water  & Forest  \\ \hline
Urban & \bf{82.90} & 0 & 1.82  \\
Water & 2.70 & \bf{100} & 4.43  \\
Forest & 14.39 & 0 & \bf{93.73}  \\ \hline
OA &  92.71 \% & &   \\
Kappa & 0.84 & &  \\

\end{tabularx}

\end{table}

%Feature space

%Show how effective rotation is

%random sampling and efficiency of randomness?

%Wishart comparison?

%Detailed polsar stats of the mission bay area

\begin{figure}[!htb]
\centering
\includegraphics[width=0.3\columnwidth]{Figures/RS2_SF_3Class/MissionBayDecomp.png} \hspace{0.5cm}
\includegraphics[width=0.3\columnwidth]{Figures/RS2_SF_3Class/MissionBayCrop.png}
\caption{Freeman-Durden three component decomposition analysis performed over area `X1', shown here along with a crop of the Pauli decomposition image.}
\label{fig:FDD}
\end{figure}


Enlarged crops of the classified map along with the corresponding aerial imagery acquired over San Francisco, representing the ground truth, are shown in Figure~\ref{fig:cl_crop}. The resolution of the imagery is $\sim0.3m$ and is significantly higher than that of the classified map. However, reasonable correspondence can be seen.  

The neighborhood named `South of Market', indicated by `X1', in Figure~\ref{fig:cl_a} with a white polygon is angled $\sim15^\circ$ from the radar line of sight. The nature of return from this area appears to be dominantly volumetric due to cross-polarization. This is evident from the Pauli decomposition shown in Figure~\ref{fig:FDD} image where it appears green.  % add polarimetric signature? 
The commonly used Freeman-Durden three component decomposition~\cite{freeman1998three} is performed to quantify the scattering mechanisms and the pixels from area `X1' are analyzed and plotted in Figure~\ref{fig:FDD}. The area has a dominant volume scatter power ($P_V$) of $\sim75 \%$, followed by even bounce scattering power ($P_D$) of $\sim17 \%$ and odd bounce scattering power ($P_S$) of $\sim 8 \%$. Such behavior is usually observed in forested areas~\cite{antropov2011volume} and such areas represent a formidable challenge to classification algorithms. Due to the rotation of T-matrices used to generate synthetic data and it's incorporation in the classification process by the auto-encoder, we see in Figure~\ref{fig:cl_a} that the entire area is correctly classified. 

`Port Oakland' consists of a some urban structures, docks and an airfield as seen in Figure~\ref{fig:cl_b}. The airfield `X2' is marked with a white polygon and is misclassified. Airfields consist of thin strips of tarmac laid on flat grass surfaces, which can be thought of as a lightly vegetated area. The tarmac is smooth compared to the radar wavelength, and waves that strike it undergo specular reflection, returning very little power to the sensor. Also, the resolution of the radar is fairly coarse, making the structure hard to classify. Surrounding areas, like the port and other buildings are correctly classified. 

John McLaren Park is surrounded by dense urban structures which are angled away from the line of sight of the radar as seen in Figure~\ref{fig:cl_c}. As above, the return from both the oriented urban areas and the park are dominantly volume scatter, making the task of classification challenging. But due to incorporation of the rotated target information, the park, marked with a white polygon and `X3', is distinctly separated. The shape of the park is well captured, and is separated from the surroundings. San Francisco Airport (SFO) is shown in Figure~\ref{fig:cl_d}. As before, the airfield is misclassified, however the surrounding buildings are correctly classified. The terminal building is marked with a white arrow in both the aerial and classified image. It has a complex and distinct shape which is clearly seen. Merced Lake is a water-body is surrounded by vegetation, and as seen in Figure~\ref{fig:cl_e}, it is correctly classified. The lake structure has been outlined with a white polygon. `Sunset Boulevard', shown in Figure~\ref{fig:cl_f}, is a dense urban area which is almost perpendicular to the radar line of sight. There is a strip of vegetation, the San Francisco Botanical Gardens, `X4' which separates two neighborhoods of dense urban structures. Centrally in the neighborhood, there are smaller parks and patches of vegetation present, with are marked with white polygons. They are seen to be have been correctly classified. 







\begin{figure}[!htb]
	\centering
	\begin{subfigure}[t]{0.45\columnwidth}
	\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/OUTPUT_3x3.png}
	%\caption{Classified map of San~Francisco.}
	\caption{}
	\label{fig:output1}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.45\columnwidth}
			\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/TrainingAreas2.png}
			%\caption{Training Areas and Areas detailed in Figure~\ref{fig:cl_crop}. }
			\caption{}
			\label{fig:train1}
	\end{subfigure}
	\caption{Figure \ref{fig:output1} shows the Classified map of San~Francisco and Figure \ref{fig:train1} Training Areas and Areas detailed in Figure~\ref{fig:cl_crop}. }
			\begin{tabular}{clclcl}
						\includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Blue.png} & Water &		\includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Green.png} & Forest &  	\includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Red.png} &  Urban 
			\end{tabular}
	%\begin{subfigure}[t]{0.48\columnwidth}
	%	\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/TrainingAreas2.png}
	%	\caption{Training Areas}
	%	\label{fig:output}
	%\end{subfigure}
	
	%\begin{subfigure}[b]{\columnwidth}
%	\begin{tabular}{clclcl}
%				\includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Blue.png} & Water &		\includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Green.png} & Forest &  	\includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Red.png} &  Urban 
%	\end{tabular}
%	\end{subfigure}
%	\begin{subtable}[b]{\columnwidth}

%	\end{subtable}
\end{figure}

\begin{figure*}
\begin{subfigure}[t]{0.16\textwidth}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/SouthOFMarket} 
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/SouthOFMarket_Cl}
\caption{South of Market}
\label{fig:cl_a}
\end{subfigure}
%\begin{subfigure}[t]{0.31\columnwidth}
%\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/SouthOFMarket_Cl}
%\caption{South of Market}
%\end{subfigure}
\begin{subfigure}[t]{0.16\textwidth}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/OaklandPort}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/OaklandPort_cl}
\caption{Port Oakland}
\label{fig:cl_b}
\end{subfigure}
%\begin{subfigure}[t]{0.31\columnwidth}
%\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/sfo_cl}
%\caption{Richmond}
%\end{subfigure}
\begin{subfigure}[t]{0.16\textwidth}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/jmpark}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/jmpark_cl}
\caption{McLaren Park}
\label{fig:cl_c}
\end{subfigure}
%\begin{subfigure}[t]{0.31\columnwidth}
%\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/merced_cl}
%\caption{San And. Lake}
%\end{subfigure}
\begin{subfigure}[t]{0.16\textwidth}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/sfo}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/sfo_cl}
\caption{SFO Airport}
\label{fig:cl_d}
\end{subfigure}
%\begin{subfigure}[t]{0.31\columnwidth}
%\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/BotGard_cl}
%\caption{Sunset Blvd.}
%\end{subfigure}
\begin{subfigure}[t]{0.16\textwidth}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/merced}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/merced_cl}
\caption{Merced Lake}
\label{fig:cl_e}
\end{subfigure}
%\begin{subfigure}[t]{0.31\columnwidth}
%\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/OaklandPort_cl}
%\caption{Port Oakland}
%\end{subfigure}
\begin{subfigure}[t]{0.16\textwidth}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/BotGard}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/BotGard_cl}
\caption{Sunset Blvd.}
\label{fig:cl_f}
\end{subfigure} %
%\begin{subfigure}[t]{0.31\columnwidth}
%\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/jmpark_cl}
%\caption{Port Oakland}
%\end{subfigure} 
\caption{Crops of the classified map shown alongside corresponding Aeriel imagery courtesy ESRI/Bing Maps.}
\label{fig:cl_crop}
\end{figure*}




		

\subsection{Comparison with Wishart Supervised Classification}

The $\bm{T3}$ matrix derived from a multilooked polarimetric image has been shown to follow the complex Wishart distribution~\cite{lee2009polarimetric} allowing the use of a maximum likelihood classification technique. This scheme is widely used in radar polarimetry both in a supervised\cite{lee2001quantitative, ainsworth2009classification, Silva2013,Turkar2013Classification} and un-supervised manner~\cite{lee1999Unsupervised,ferro2001unsupervised}. However, this scheme is not able to account for urban areas rotated to the line of sight. This is evident in the classification result presented in Figure~\ref{fig:compw}. The proposed method is able to correctly classify the `South of Market' area, marked by `A' in the figure, while it is mis-classified in the result from the Wishart Supervised classifier. This is because the Wishart classifier relies on the scattering statistics from only one $\bm{T3}$ matrix, where as the proposed method incorporates information from the rotated $\bm{T3}$ matrices via the auto-encoder stage. When the $\bm{T3}$ is rotated the angle that the urban structure makes with the radar line of sight, more even bounce scattering is observed as compared to the unrotated  $\bm{T3}$. The proposed method is about to utilize this information to correctly classify the area. This is also observed in the urban neighborhoods of `Oceanview' and `Outer Mission', marked by `B'. The Wishart classifier is unable to classify the neighborhood correctly, but the proposed method successfully identifies this area as urban, While accurately classifying the adjacent John McLaren Park as vegetation land-cover type.  

The confusion matrix analysis for the Wishart supervised classification is presented in Table~\ref{tab:wishtab}. The overall accuracy achieved with this technique is $79.30\%$. There is significant confusion between the urban and forest areas, with $\sim30\%$ urban pixels being classified as forest. This is also evident in the classification images. This is however not a reciprocal phenomenon, and in the reverse case, only $\sim16 \%$ of forest pixels are mistakenly classified as urban. The error in separation of water pixels is also slightly higher. This is because slight ghosting from the high return area corresponding to the dense urban area can be seen in the image, which is misclassified as urban. These appear as single urban or forest pixels in a water pixel region, having a speckle like appearance.Due to the incorporation of statistical information in the second stage of the proposed technique, we are able to use the neighborhood window information to suppress such errors, leading to better accuracy. 

\begin{table}[!h]
\centering

\caption{Confusion matrix and accuracy statistics for Radarsat-2 San Francisco, CA data-set using Wishart Supervised Classification}
\label{tab:wishtab}
\begin{tabularx}{\columnwidth}{X|XXX}
 & Urban & Water  & Forest  \\ \hline
Urban & \bf{62.80} & 0 & 16.82  \\
Water & 4.55 & \bf{96.2} & 6.45  \\
Forest & 32.65 & 3.8 & \bf{80.73}  \\ \hline
OA &  79.30 \% & &   \\
Kappa & 0.72 & &  \\

\end{tabularx}

\end{table}

\begin{figure}
\centering
\begin{subfigure}[t]{0.45\columnwidth}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/Wishart_Crop}
\caption{Wishart Supervised}
\end{subfigure}
\begin{subfigure}[t]{0.45\columnwidth}
\includegraphics[width=\columnwidth]{Figures/RS2_SF_3Class/Deep_Crop}
\caption{Proposed}
\end{subfigure}
\label{fig:compw}

		\begin{tabular}{clclcl}
					\includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Blue.png} & Water &		\includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Green.png} & Forest &  	\includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Red.png} &  Urban 
		\end{tabular}
		
		\caption{Comparison of the Wishart Supervised maximum likelihood classifier and the proposed method.}
\end{figure}


\section{Conclusion}
* Advantage of rotation + novelty
* Advantage of muller
* performance of SF and LA images
* future: multiangle, multi freq, transfer





% References should be produced using the bibtex program from suitable
% BiBTeX files (here: strings, refs, manuals). The IEEEbib.bst bibliography
% style file from IEEE produces unsorted bibliography list.
% -------------------------------------------------------------------------
\bibliographystyle{unsrt}
\bibliography{References/compact_classification_sar,References/deepLearning,References/polsar,References/IGRASS2015,References/ML,References/polsar_stats,References/remoteSensing,References/SARobjectrecognition,References/ml_books,References/bigdata,References/urban,References/uavsar}


\end{document}
