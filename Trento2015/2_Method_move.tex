Current and future SAR missions are designed to image Earth's surface, systematically generating spatially and temporally dense datasets. This leads to massive volumes of collected data, which is difficult to analyze manually by human analysts. To successfully leverage learning and information from the dataset, it is critical to utilize automated or semi-automated machine learning techniques. The advantage of using a deep learning based scheme like described in this work is that the network can be trained incrementally on a large volume of data, and once the network has been trained and set up, the actual classification of unseen data is computationally efficient and fast.

%DL algorithms
\begin{figure}[!htb]
\centering
		\includegraphics[width=0.9\columnwidth]{Figures/Trento/OverallDL}
		\caption{A general framework for pixel classification of polarimetric SAR images using a deep learning architecture.}
		\label{fig:GenDL}
\end{figure}

A general framework for pixel classification of polarimetric SAR datasets is shown in Figure~\ref{fig:GenDL}. The bands of the polarimetric image can be considered as low-level input features that describe the geometry and dielectric properties of the target. However, classification done at this stage would not be robust enough to perform acceptably in a general situation. A pre-processing step is generally applied to transform these into mid-level features.  These generated features are more robust, but volume expands many-fold. A deep learning network is used to extract high-level features which are both efficient and robust from these mid-level generated features. In literature both supervised structures such as CNNs~\cite{hu2015deep} and unsupervised structures such as auto-encoders~\cite{chen2014deep}, DBNs~\cite{chen2015spectral} are used. The mid- or low-level features are introduced to the network at the bottom layer and features are extracted from the top or intermediate layers.  The extracted features are then classified using a suitable supervised classifier. In general, there are two kinds of classifiers. Hard classifiers such as SVMs~\cite{lagrange2015benchmarking} which directly produce the classification label as output at every pixel, and soft classifiers such as logistic regression that can fine-tune the whole pre-trained network and predict the class labels as a probability~\cite{chen2015spectral,chen2014deep}.


%----
\subsection{Autoencoder} ~\label{sec:stage2}


Consider a learning problem where a labeled training set $(x^{(i)},y^{(i)})$ is available. A neural network consists of  individual neurons that are interconnected such that it is possible to define a complex, non-linear, non-parametric hypothesis $h_{W,b}(x)$. The parameter $W$ is the weight matrix and $b$ is the bias term. Both these parameters are fitted to the data over the training process. A `neuron` can be thought of as a computational unit that takes as input $\bm{x}_n^{(i)} = x_1,x_2 ... x_n$ and an intercept term $b$, and produces the output, called hypothesis, $h_{W,b}(x) = f(\bm{W^{\intercal}x}) = f(\sum_{i=1}^{n}W_ix_i+b)$, where $f: \Re  \Rightarrow \Re $ is called the activation function or nonlinearity. Traditionally the sigmoid $\sigma(x) =  1 / (1+e^{-x})$  has been used as the activation function. It takes a real-valued number $x$ and outputs a value with the limits $[0,1]$, according to the magnitude of the input. In practice this has some drawbacks. The output of the sigmoid saturates at either tail of $0$ or $1$ and the gradient at these regions tends to zero, which causes a very small output to be backpropogated. Additionally, if the initial weights are large, the neuron can quickly become saturated during training. The sigmoid function also is not zero centered. During back-propogation if the input data to the neuron is always positive i.e. $x>0$, then the gradient weights will either become all positive, or all negative. This could introduce undesirable oscillation dynamics in the gradient updates for the weights. To overcome the non-zero centering problem one may use the tanh activation function, $ f(x) = tanh(z) = (e^z - e^{-z}) / (e^z + e^{-z})$ which has the limits $[-1,1]$. A non-saturating nonlinearity like the Rectified Linear Unit (ReLU) activation function~\cite{glorot2011deep} can be used to overcome the saturation problem. The ReLU $f(x) = max(\epsilon,x)$ simply thresholds the data at $\epsilon$, typically $\epsilon=0$. The differentiation of the function is defined as

\begin{displaymath}
   \frac{df}{dx} = \left\{
     \begin{array}{lr}
       1 & : x > \epsilon\\
       0 & : x \leq \epsilon
     \end{array}
   \right.
\end{displaymath}

The training time for saturating activation functions is larger with the gradient descent algorithm than non-saturating counterparts~\cite{krizhevsky2012imagenet}. Because deep learning algorithms use several layers, faster training of ReLU units as compared to sigmoid or tanh translates to multi-fold reduction in training times~\cite{nair2010rectified}. The computation of the output of a ReLU is computationally simple as it does not involve evolution of exponentials like in the sigmoid and tanh, allowing for in-place computation and requiring less computer memory. 

A disadvantage of ReLU units are that when subjected to large gradients they can update their weights to such a state that they can not be activated by subsequent inputs. The neuron is said to 'die' in the training. To overcome this Leaky ReLU units, $f(x) = 1(x<\epsilon)(\alpha_r x) + 1(x \ge \epsilon)(x)$ can be utilized. Such units have a small negative slope, $\alpha_r$ when the input is below the threshold, i.e. $x<\epsilon$. This allows the neuron to recover even if it's weight has been updated to a high value by a particular input, over subsequent inputs. The differentiation can be defined as


\begin{displaymath}
   \frac{df}{dx} = \left\{
     \begin{array}{lr}
       1 & : x > \epsilon\\
       \alpha_r & : x \leq \epsilon
     \end{array}
   \right.
\end{displaymath}

In this work we use leaky ReLU units in the implementation of the autoencoder, as described subsequently. Typically, and in this work $\alpha_r = 0.01$.

An auto-encoder is a fully connected neural network who's outputs are tied to its inputs in the training phase. Thus, after training the network's weights are able to closely approximate the input data, and in the ideal case, learn a perfect representation of it. A sparse auto-encoder has a large number of hidden units per layer but only a small percentage produce significant activation. Such mechanisms have been shown to have learned more useful representations of the data than the original input~\cite{vincent2010stacked}. variations of this technique have been successfully used in different fields to extract better features from data without need for pre-training~\cite{hinton2006reducing}
~\cite{gravelines2014deep}. In this work we use a stacked auto-encoder~\cite{bengio2009learning} in the preliminary unsupervised feature learning stage. 

\begin{figure}
\centering
\includegraphics[width=0.9\columnwidth]{Figures/layers}
\caption{Something}
\label{fig:layerplot}
\end{figure}

The auto-encoder network used in this study is a 6 layer stacked sparse auto-encoder as shown in Figure~\ref{fig:method2}. The selection of the number of layers were done by training the UAVSAR data-set using the same hyper-parameters and number of nodes though different auto encoder topologies with increasing depth and measuring the lowest cross-entropy error achieved. This is plotted in figure~\ref{fig:layerplot}. We see that a 6-layer auto-encoder was found to be most optimal for this dataset. From input $x$ to output $y$, the network has $1024, 512, 256$ nodes in Layer 1, 2 and 3 respectively forming the encoder, and $128, 512, 1024$ nodes in Layer 4, 5 and 6 respectively forming the decoder. Each node has a leaky ReLU non-linearity allowing the handling of large dynamic range input without saturating. The sparsity hyper-parameter of the network is set to $\rho = 0.15$ which ensures that the average activation of each hidden neuron $j$ approximately is near 0. This is achieved with the addition of a penalty term to the objective function. 
The network is iterated $200,000$ times with a batch size of $1000$. This ensures that the complete data set is passed multiple times though the auto-encoder for better generalization. To ensure that the learning machine is not memorizing the order in which the data is being fed, the order of the data points are randomized. The base learning rate $\alpha = 0.001$ and the update step multiplier $\Gamma = 0.1$. The step-size is chosen to be $40,000$ to allow for sufficient number of epochs before the learning rate is updated. The gradient descent momentum is $\mu = 0.85$. The value of batch size is adjusted depending on the size of the image. The values described here are typically for a $1000\times1000$ pixel crop.

%* why six layers

%* why relu nodes

%* what sparsity

%* what is extracted

The weights of the nodes of the third decode layer, i.e. Layer 6, are extracted after completion of the training phase and used as features for classification for the next step. They can be considered to be combined information from the $\bm{T3}$ matrix, and all the considered rotations that have been considered. Intuitively the last layer of the decoder can be assumed to have the most captured information, however we qualify this assumption with a test. Two $25\times25$ pixel patches, one over the urban area (Area A) and the other over the forest area (Area B), $S_M = (s_1,s_2,s_3,\ldots,s_n)\  and\ R_M = (r_1,r_2,r_3,\ldots,r_n) \in \Re  $  respectively, are selected as test patches as shown in Figure~\ref{fig:seperation}. The Minkowski distance between the two patches is computed as:

\begin{equation}
\left( \sum_{i=1}^{n} |x_i - y_i |^p \right) ^{1/p}
\end{equation}

For $p \geq 1$ the Minkowski distance is a metric~\cite{lukaszyk2004new} which can be used to evaluate the dissimilarity between $\bm{S_M}$ and $\bm{R_M}$. The more the distance the greater the higher the separability between the two classes. From Figure~\ref{fig:seperation} we see that the lowest distance is achieved in Layer 4, hence the most information is encoded in this layer. This layer is selected for feature extraction from it's 128 nodes.

The resultant feature-space has a far lower dynamic range than the original data-space and also combines the information from multiple synthetic rotations, allowing better performance in the final classification by subsequent stages. Also, the probability of convergence of the next stage is improved. 
 
\begin{figure}

\begin{subfigure}{\columnwidth}
\centering
	\includegraphics[width = 0.85\columnwidth]{Figures/Trento/seperation}
\end{subfigure}
\par\smallskip % maximise vertical space here instead
\begin{subfigure}{\columnwidth}
\centering
	 \includegraphics[width = 0.5\columnwidth]{Figures/Trento/seperation2}
\end{subfigure}   
	\caption{The Minkowski distance between test area A (Urban) and B (Forest) is shown (top). The regions evaluated are overlaid on the Pauli RGB (bottom)}
	 \label{fig:seperation}
\end{figure}

%* why d3 layer  - fig



%\subsubsection{Describe the feature space}

% This was cut away from section on MLP - Move to experimental section

A demonstration of the learning of the network is carried out on a $500\times500$ pixel crop of the UAVSAR L-band data-set collected over Oakland, CA. For this test, the rotated urban areas are treated as a separate land cover class to allow us to observe the behavior of the network. The evolution of the network and the progress in classification is shown in Figure~\ref{fig:evolution}. The network first learns the most prominent feature, the urban area in the line of sight of the radar. It is initially misclassified as forest, but the high error is back-propagated and it evolves to classify it correctly. After about $10,000$ iterations, the network begins to learn the rotated urban areas. The rest of the duration, as the learning rate $\alpha$ steadily decreases, the network gradually learns to correctly classify the rotated urban areas, which are initially misclassified as forest. Radar shadow effects are a consequence of the side-looking geometry of radar, and they are limitations of the imaging medium. The shadow regions have not been masked for this demonstration and shows up as a misclassification in the forest region as indicated by the white arrow in Figure~\ref{fig:evolution}. Since this classification is performed only to understand the behavior of the network, the effect of shadow regions are temporarily ignored.  It is reasonable to assume that the rotated urban areas and forest will be the most likely confused targets. This is because cross-polarized return from even bounce targets oriented away from the radar line of sight appear very similar to the returns from the forest areas. On measurement of the final classified image in the subsequent section, we see that this assumption is true.   

\begin{figure}[tb]
	\centering
	\includegraphics[width =0.9\columnwidth]{Figures/Trento/EvolutionOFNN}
	
%	\vspace{0.1cm} 
%	\begin{tabularx}{\columnwidth}{l l l l l l l X}
	\begin{tabular}{l l l l l l l l}
					\includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Blue.png}& Water & \includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Brown.png} & Forest & \includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Yellow.png} & Urban & \includegraphics[width=0.03\columnwidth]{Figures/RS2_SF_3Class/Cyan.png} &  Rot. Urban 
%	\end{tabularx}
	\end{tabular}
		
		\caption{Evolution of the MLP over iterations is shown for a crop of the UAVSAR L-Band data-set collected over Oakland, CA. Four classes are used to demonstrate the learning action of the neural network.  }
	 \label{fig:evolution}
\end{figure}